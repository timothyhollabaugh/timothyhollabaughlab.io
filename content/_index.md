+++
title = "About Me"
+++

I'm a robotic software engineer and serial tinkerer. I love to build things, take things apart, and solve problems.
You can read about some of the things I have done here.
