+++
title = "OPEX"
date = 2020-09-28
[extra]
    image = "opex/opex.png"
+++

At one of Rowan's career fairs, I noticed a table in the back with a blue cloth, displays playing videos, and, most
importantly, what looked like a robot of sorts proped up on the table. There was a line forming, so I quickely joined
in. Getting a little closer, I saw the name of the company, OPEX. Watching the videos and reading the posters, it
looked like they did robots in warehouses. I was intrigued. To make a long story short, I was quickly set up for and
interview, and later a summer internship.

[OPEX](https://www.opex.com) is a warehouse and mail automation company. I was working in the warehouse automation part,
where they make machines for warehouses that handle product sorting and storage. Their big thing is that the bots, known
as iBots, run in a contained frame that includes a grid of bins. Their smaller system, SureSort, scans incoming items
up the wazzo to figure out what they are, then the bots carry the items to the right bin. Operators can later empty the
bins from the outside. Their larger system, Perfect Pick, is used for product storage. The bots carry entire bins up to
the presentation station abd back, where the operator can pick and put items. It will even integrate well into the
warehouse's conveyer system to tell the operator what to pick, where it is, and where it goes.

{{ images(images=['ibot.jpg']) }}

When I started, I was working on their GUI for PerfectPick. This gets presented to the machine operator, and tells them
what to pick. I did a bunch of work with JavaFX, fixing up a few minor issues. Later on, I got moved to more embedded
work. This time, I was writing C++ to run on an embedded Windows box with an RTOS. In fact, I wrote the initial code for
this system, both the RTOS controller code and the Windows host program and the communication between them. I set it up
so the controller would be in charge of a block of shared memory that included almost all of the controller's state.
There was also a set of mailboxes for the contoller and host to synchronize updating the shared memory, and so the host
can tell the controller what to do. The only way for the host to do anything is to send a message through the mailbox;
it cannot modify the shared memory directly. This means the synchronization is much easier: the host only needs to avoid
reading while the controller is writing. There is no need to deal with writing at the same time or reconciling differing
states. Having the entire state in shared memory also makes debugging a lot easier. The only other way to know what is
going on in the controller is through printing to the console or the log file. The shared state let us writing a host
GUI to see what is going on in real time. This ended up working pretty well.

It turns out that the people at OPEX liked me a bit, so they asked if I could come back for winter break and the next
summer. Of course I agreed. Over winter break, I continued to work on the embedded Windows and RTOS code, fixing up a
bunch of small things that they didn't get around to yet.

Over the second summer, I got to work on something new. There was a new board with an STM32F4 microcontroller that
connected to the embedded Windows box, and it needed code. It basically acted as a fancy IO expander for the Windows
box, and talked through to the RTOS controller over USB. The STM32 program wasn't that hard: it just read some bytes
from USB and set outputs accordingly, and the reverse for inputs. The RTOS controller turned out to be a bit more
involved. The first plan was to have the controller talk directly over USB. However, due to the was the RTOS worked,
trying to give the RTOS control of the USB port required passing over the entire USB controller. Guess what else is on
that USB controller? The mouse, keyboard, and USB dongle for the RTOS licence. Not having a mouse and keyboard made
Windows pretty unusable while debugging, but the the licence dongle make the RTOS pretty useless and defeated the
entire point. As a side note, the only way to fix Windows not having USB is to reinstall Windows; nothing else can
really be done. The solution was to have the Windows host program handle the USB connection, and pass the info back and
forth to the controller. This was a bit of a speed bump, but nothing too complicated. The controller would then handle
these messages and integrate them with the rest of the IO. By the end of the summer, everything was working pretty well.

Overall, I enjoyed this internship. Before this, I had only used C++ in passing, without really understanding a lot of
it. Here, I got to really learn a lot of C++, even some of the more arcane bits regarding the RTOS. Speaking of RTOSes,
this was also my first time using an RTOS. All of my previous projects were simple enough that some bare-metal
microcontroller code was all I needed. However, OPEX had a lot more going on. The whole host/controller thing was
something I had sort of done a few times before, but nothing this invloved with shared memory involved. This did end
up being somewhat similar to what I did with the [micromouse debugging](/projects/micromouse) debugger, however.
