+++
title = "Large Tricopter"
date = 2016-03-27
[extra]
    image = "largetricopter/largetricopter.jpg"
+++

{{ images(images=['largetricopter.jpg']) }}

My first tricopter was based off of the rcexplorer, except that I 3d printed the body parts and motor mounts. After many hardships in getting that flying, I had decided that I wanted something smaller, that would be more acrobatic. Because I had already bought the parts for this one, I redesigned an entirely new frame that was as small as I could make it without the propellers hitting anything. As a result, everything is oversizes for the frame, and it is a little clunky to fly. I have added a GPS to it to allow for return to home functions, but I have no switches on my remote to activate it!
