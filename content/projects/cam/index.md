+++
title = "3D CAM"
date = 2024-09-22
[extra]
image = "cam/cam.png"
+++

{{ images(images=['cam.png']) }}

[Code on GitLab](https://gitlab.com/timothyhollabaugh/cam3d3)

Now that the toolchanger has a [spindle tool](/projects/3dprinters/toolchanger/spindle-tools/), it needs gcode generated
to cut anything useful, which is usually generated with a Computer Aided Manufacturing (CAM) program. Since there's no
good CAM for Linux and I wanted to generate gcode for multiple tools in the same job, I decided to have a crack at
writing my own. I had previously written a [2D CAM program specifically for PCBs](/projects/pcbmill/cam/) which works
well for that, but now I wanted to go 3D. Of course it would be written in Rust again, and I could pull over all the
2D geometry and toolpath generation code from the 2D CAM. However, I switched the GUI from druid to egui + wgpu so I
could render what's going on in 3D.

After many false starts and a lot of work, it's finally to a workable, albeit very pre-alpha and limited state. You can
try it out from the GitLab repo above, and it should be a matter of [installing rust](https://rustup.rs) and running
`cargo run --release`. Make sure you have a vulkan GPU driver installed, and if you get errors about a present mode
starting up, edit the present mode in `cam3d-ui/src/main.rs` on the line `present_mode: wgpu::PresentMode::Immediate` to
one of the present modes it specifies in the error message. If you're on Windows, you'll need to edit `cam3d-ui/Cargo.toml`
on the line that starts with `clipper-rs = {` to
`clipper-rs = { git = "https://gitlab.com/timothyhollabaugh/clipper-sys", branch="msvc" }`.

# Slicing

My first thought to turn 2D into 3D was to slice the model at fixed intervals up like 3d printing slicers do and just
run the 2D algorithm on each slice. I wrote myself a function to [intersect a mesh with a plane](https://gitlab.com/timothyhollabaugh/cam3d3/-/blob/main/cam3d-ui/src/cam/geometry3d/solid.rs?ref_type=heads#L73),
which works well but fails when the model has a surface at the plane you're trying to slice. With that, it's not too
difficult to [slice it up run the 2D algorithm for each slice](https://gitlab.com/timothyhollabaugh/cam3d3/-/blob/main/cam3d-ui/src/cam/toolpath/adaptive_3d.rs?ref_type=heads).
This gets the job done, but has some limitations.

For one, the model will likely have horizontal surfaces that aren't
on one of the slices. Since the slices can be much thicker than 3D printing slices, this means either being limited in
which horizontal surfaces you can design for, or slicing with a ton of small slices and it takes forever
(both to calculated and mill). I ended up writing some code to look though all the faces in the mesh, and for any that
are horizontal enough, inserting a layer at that z location. Then after slicing, it needs to go though only keep the
inserted slice where there's a matching surface in the model.

Second, when milling, you want to do each pocket all the way down before moving onto the next one, instead of jumping
between all the pockets on one slice before moving to the next one. This can reduce travel time significantly. To handle
this, after slicing, it goes though all the slices and organizes them into a tree based on how many individual polygons
are in the slice below any given slice. Then, they can all be done in a depth-first-search order.

# Adaptive Toolpaths

One of the limitations of doing milling on essentially a beefed-up 3D printer is that the machine has the rigidity of a
wet noodle. This means milling slower in general, but it also means lowering the stepover from one pass to the next can
have a huge effect. Especially when trying to do a full-width slot, the endmill can bounce around in the slot between
the two walls, which just makes a bad situation worse. Unfortunately, the 2D algorithm I came up with before tends to
full-width slot a lot when it first starts a pocket. It even does the perimeter of the pocket first, so the extra chatter
from the slot ends up on the finished walls of the part. To avoid this, there's a toolpath generation strategy known as
Trochoidal Milling. You first plunge or helix-ramp the tool in the center of the pocket, then spiral outwards, and branch
off into the corners of the pocket. The whole time, the tool is cutting with a circular motion, and never engages more
than the set stepover with the material. 

Actually doing this is easier said than done. I might give this a full page later, but the short answer is
1. Generate the medial axis of the given polygon using a Voronoi diagram. This gives the line that is directly in the
    middle of the polygon, along with a circle radius for each point on the line of the circle that touches the polygon
    outline.
2. Filter the medial axis to remove segments outside the polygon or too small to be useful.
3. Choose a starting point based on the largest circle in the medial axis.
4. Figure out what order to mill the medial axis using A-Star.
5. Step through the axis in order and generate circular arc toolpaths ever stepover-distance along it.
6. Generate a finish pass around the perimeter of the polygon to clean up and bits left over

This works pretty good. It allows the toolchanger to mill parts that would be impossible without it. However, it's not
perfect. For example, the circular toolpaths spend a lot of time cutting with less than the specified stepover, so it
ends up taking longer than it should.
