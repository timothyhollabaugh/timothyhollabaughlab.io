+++
title = "Micromouse V0"
date = 2018-12-30
[extra]
    image = "micromouse-v0/micromousev0.jpg"
+++

{{ images(images=['micromousev0.jpg']) }}

In order to compete in the competition in April, I decided to start early in September to give plenty of time. Since this was my first micromouse, I tried to keep the components simple and what we had in stock.

Stepper motors were used for motion, since they do not require encoders and can be controlled precisely. They were driven with DRV8825 stepper drivers the same as my 3d printer. The stepper motors worked enough to move, but they had some problems. To start, the drivers required at least 8.2v to operate. This meant a relativly large battery. But a bigger issue was that the stepper motors were pretty slow and skipped steps often if driven quickly or there was anny resistance to movement. This made moving quickly quite difficult. It probably would help to implement acceleration to get to a higher speed.

For sensing the walls, I used ultrasonic sensors, mainly because I had some available. There were 3 of them, one on the front, left, and right of the mouse. This would allow the mouse the sense the wall around it and center itself between the walls. The ultrasonic sensors used two pins to operate: one to trigger a pulse, and one that goes high for a time proportional to the distance. Unfortunatly, the interrupt pins on the atmega328 I was using were limited, so I ended up using a 4:1 mux to be able to switch between the three sensors. This worked pretty well, and ensured that no two sensors were pulsing at the same time to reduce interference. However, it turns out that the range and accuracy of ultasonic sensors is pretty limited. Since the walls on the maze are only about 18cm apart, there was only a few centimeters of room to each side of the mouse. The ultrasonic sensors were able to sense if there was a wall there, but were not good enough to tell how far away it was reliably.

The micromouse was powered from a 6 pack of AA batteries to ensure that there would be no lipo fires, and because they were available. 6 AA batteries provided about 9 volts, above the minimum voltage of 8.2 volts on the stepper drivers. This worked as long as the batteries were fully charged, but it did not take long for the voltage to drop below 8.2 voltages and cause a lot of missed steps. The batteries were also large and heavy, which didn't help moving quickly.

The control was provided by the an atmega328pu on a custom milled board, again, because I had them available. This board includes the atmega, the stepper drivers, a voltage regulator, the ultrasonic sensor mux, some buttons, and some leds. Not everything would fit onto one board, so they were split into two boards and stacked. This worked pretty well. The only issue was that the atmega328 was not really powerfull enough to deal with the sensors, motors, and do maze solving. All three timers were used for the motors and ultrasonic sensors, and there was bearly enough ram to hold an entire maze.

Overall, I was able to get this mouse to move around, sense walls, and have a nice command line interface over UART. This was enough to make me change my mind about using only parts that we had, so I decided to redo it in the spring semester.

