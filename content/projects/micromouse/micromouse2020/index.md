+++
title = "Micromouse 2020"
date = 2020-04-14
[extra]
    image = "Profile.png"
+++

{{ images(images=[
    'IMG_1262.JPG',
    'IMG_1305.JPG',
    'IMG_1306.JPG',
    'IMG_1290.JPG',
    'IMG_1283.JPG',
    'IMG_1288.JPG',
    'IMG_1275.JPG',
])}}

After getting very lucky at the 2019 SAC competition, we figured it was time to take luck out of the equation. After do
a little research and watching some videos of national and world level competitions, I decided that we needed to really
up our game to compete beyond out local competition, more than just a navigation algorithm. Mice would travel at speeds
averaging around 2m/s, and peaked at almost 5m/s. Mice would perform smooth curves around turns, and never stop. They
would map out the maze in it's entirety, then whip through it at high speed. The fastest mice would make it to the
center in only a few seconds. Our mouse stopping at every turn and taking its sweet time would not cut it.

# Goals

Last year, the goal was to build good hardware that would allow the software to improve later on. This year, I wanted to
do the same thing, but at a higher level. One of the things that let other mouse go so fast was to do curves around the
turns. This means that the mouse does not need to spend time decelerating and re-accelerating every cell; it can just
keep going. If we have the software to do curves, then we can play the speed optimization and tuning game later. Thus,
this year's main goal is to do _curves, without stopping_. Of course, there will be other things that get done along the
way. A better debug interface would be helpful, and there are a few hardware changes that can be made.

# Hardware

While the 2019 hardware worked pretty well, a few things were learned. The motor driver needed pull down resistors on
the inputs, one of the I2C lines was backwards, and we didn't really need the second battery. So the goals are to take
what we had, and tidy it up a bit. Since the only need one battery, the mouse is now half the size. I moved to a four
layer board to make routing easier on the smaller mouse, and to provide continuous ground and power planes. I also added
a footprint for an accelerometer and gyroscope to help the control software, but it turned out that the footprint was
the wrong size. Oops.

# Software

Do do curves, a lot of the current assumptions that the code makes become invalidated. The current code pretty much
always assumes that the mouse will come to a stop after each move, and that it is always going straight through the
middle of a cell. I also wanted to change the way the code was structured: instead of nesting control structures
deep inside each other, and getting passed direct references to other control structs, I wanted to try a more horizontal
layout, with the different modules being more independent of each other. I decided to completely rewrite all the code,
keeping a bit of the hardware interface. This will be described in another post.

Another thing I wanted to improve upon was the debugging methods. Since the mouse is very mobile and tied to hardware,
it is difficult to use a traditional debugger. Stopping the processor does not stop physics from happening around it,
and any sort of tether would not work. Last year, I used a bluetooth serial module to send back real time data, and
plotted it on my laptop. This worked great, but was rather low-bandwidth, manual, and difficult to visualize. If I
wanted to get curves working, I would need a better way to know what is going on. What I ended up with is described in
another post.
