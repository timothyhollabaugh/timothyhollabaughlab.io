+++
title = "Toolchanger"
sort_by = "date"
[extra]
    image = "toolchanger/IMG_20230618_201656484.jpg"
+++

{{ images(images=[
    'cad.png',
    'IMG_20230618_201617792.jpg',
    'IMG_20230618_201627157.jpg',
    'IMG_20230618_201656484.jpg',
    'IMG_20230618_201733641.jpg',
    'IMG_20230618_201743534.jpg',
    'IMG_20230618_201800575.jpg',
    'IMG_20230618_201807858.jpg',
]) }}

[CAD in Onshape](https://cad.onshape.com/documents/e727312230e5e7804f1506d2/w/dbc694c1bf65abc33bc64ff1/e/deddb6850c62af683b89cca1?renderMode=0&uiState=648fca990fdfa0779e52c588)

Before I go too deep, here's some quick specs:
 - 4 tool docks
 - Modified E3D tool latch mechanism
 - 2 direct drive Revo Micro print tools, 7 pen tools, a drag knife tool, and a spindle tool built up so far
 - Approx 235x170x90mm work volume
 - CoreXY motion
 - Klipper
 - 12v heated bed but running on 24v, with an FR4 surface
 - BTT EBB36 toolhead boards connected over CAN
 - Partially enclosed for now
 - 400mm/s moves and 30000mm/s^2 accel
 - 600w 24v supply

This is very much still a work in progress, so the above may change.

## Background

Back in 2018, E3D announced they were working on their [Toolchanger project](https://e3d-online.com/blogs/news/research-and-development-motion-system-and-tool-changer?_pos=18&_sid=be50472f3&_ss=r)
This is a 3d printer that will automatically swap out the entire print head, with different print heads for different
colors, materials, nozzle sizes, etc. In fact, the tools don't event need to be print heads, and can do anything you want.
E3D has talked about milling tools and pick-and-place tools used in conjunction with the hotend tools in the same print,
allowing things like [ASMBL](https://e3d-online.com/blogs/news/asmbl?_pos=1&_sid=7112281b8&_ss=r) or inserting nuts and
screws in the middle of a print. E3D don't even call their toolchanger a 3d printer. Instead, it's a _Motion System_.

When I first saw this, my mind was immediately blown. Like, there's so much you can do with this. I had been dealing
with multimaterial 3d printing for a while -- my first printer was even a Tricolour Mendel, and I had been following the
Prusa MMU closely. Swapping the entire hotend and extruder in one go solves nearly all the problems with both
many-nozzle-one-carriage like my original Tricolour, and many-filaments-one-nozzle systems like the MMU:

 - Only one nozzle moving over the print eliminates ooze messes and mechanical z-alignment
 - Each hotend can be a different temperature for different materials
 - Each hotend can be a different nozzle size
 - No purging filament between swaps

And since E3D mentioned milling tools and pick-and-place heads, and I was working on my own [PCB mill](projects/pcbmill),
my brain immidiatly went to making PCBs. You could have a few milling tools with different bits to mill PCBs, along with
a solder paste syringe and a pick-and-place tool to asseble it, all in one go. Maybe you could even fit in a soldermask
tool of some sort and a reflow hot plate, and now you have a single machine that will bring copper clad and components
all the way to a professional level PCB. Of course, I knew this would be rather difficult to pull off, and there are
several hard problems to be solved. Not to mention that this would only be useful for simple boards in small volume.

All that being said, I eventually decided to build my own toolchanger. Why build my own instead of buying E3D's? Well,
the E3D toolchanger is rather expensive and quite a bit larger than I would want, and besides, building printers is
always more fun. Why not?

Since I knew the PCBs idea was a bit of a stretch, I decided to aim for just hotend tools at first, but to avoid designing
myself out of doing PCBs or other such experimental ideas. As such, the following requirements were born:
 - 4 tools. The E3D machine is 4 tools, and 4 tools get you 2 mills + solder paste + PnP for PCBs. Also 4 colors in a
     print just seemed like a good number.
 - Fit two 4x6in blank PCBs on the bed, which is ~150x200mm. This leaves space for 1 PCB + mounting + parts for PnP.
 - Be reasonably stiff (more like not-a-wet-noodle) for milling

Some other goals, but not hard requirements:
 - Be as small as possible while still fitting 4 tools and the PCBs. This will increase stiffness, decrease cost, and
    generally reduce large printer problems.
 - CoreXY motion system. This keeps the motors off the moving parts, and I knew it would work well with the toolchanging
    mechanism since the E3D toolchanger uses it. Also it's pretty cool. Unfortunately the long belt paths aren't great for
    stiffness, but if I keep the printer small it's probably fine.
 - Be relatively cheap and easy to put together. I don't have access to a lot of metalworking tools.

_Note: I'm not great at taking pictures of things as I go, so there'll be lots of CAD screenshots_

