+++
title = "Extruder Tools"
date = 2023-09-28
[extra]
image = "extruder-tools/revo_extruder.png"
+++

The extruder tools were the first tools I did for the toolchanger. At first, there was just one bowden knock-off V6 tool.
That worked good enough, but it was really just a stopgap until I could do them for real. Now, there are two direct-drive
Revo tools, and more are planned.

# Direct-Drive Revo Tools

[CAD in Onshape](https://cad.onshape.com/documents/6097e1dc4d53b897e284f26e/w/7864c8a9a4226afa30cf40b7/e/222bcb4c953db94c01442c01?renderMode=0&uiState=65249861eeb4c161a1538eb7)

The goal here was to fit a direct drive extruder with one of the Revo hotends onto a tool. This proved to be more difficult
than expected. The toolchanger was designed around 50x50mm tools, which was based off the size of a bowden V6. However,
I quickly came to realize that there are no direct drive extruders available that fit well in that footprint and keep
the nozzle in the middle. Since the machine was already built and I didn't want to make it any bigger, there was only
one option: to design my own extruder.

{{ images(images=['revo_extruder.png']) }}

## Gearing
Most of the Bondtech style extruders are really close to fitting. However, the stepper motor always sticks out just a
few mm too far. This happens because the drive train gearing and the gearing between the two drive wheels always sits
directly between the filament drive wheels and the motor. Instead, I could put the drive train gears and the gears that
connect the two wheels on opposite sides of the drive wheels, allowing the motor to squeeze in a few mm further.

{{ images(images=['geartrain.png', 'inside1.jpg', 'inside2.jpg', 'inside3.jpg']) }}

I settled in the Bondtech LGX drive wheels mostly because they were the thinnest. The drive train mostly looks like the
regular LGX mini, but the drive train gear has a bit of a shaft between the smaller and larger gear, allowing it to
straddle across the drive wheels. This is what allows to be more compact. I went through a few gear ratios, 1:4 and 1:16,
before settling on 1:8. I was originally using an old DRV8825 stepper driver to drive the NEMA14 36mm motor. This was
really struggling to push the filament through, so I jumped all the way to 1:16 to try and help it out. The 1:4 was using
one of the motors with a 5mm shaft that I could put my own gear on, but there wasn't a good way to go much more than 1:4
with that. The 1:16 version switched to the motor with the gear pressed on. Since this gear is much smaller than I could
do with the shaft, and the shaft is shorter, the extra gearing became possible. The 1:16 version essentially replaced the
shaft with an extra set of gears. This helped, but it wasn't enough. Now the motor had to spin rather quickly to keep up,
and it lost a lot of torque at those speeds. So I settled with 1:8. This worked a lot better, and also saved a whole gear
stage and let it squeeze down even more! Despite that, it still wasn't amazing at pushing filament. Once I went to a
TMC2209 driver, these problems went away -- that change had more of an impact than all the gearing changes.

{{ images(images=['geartrain4.png', 'geartrain8.png', 'geartrain16.png']) }}

The gears were originally printed out of PETG with a 0.15mm nozzle, and the housing all polycarbonate. This worked pretty
well at first. On the first 4:1 version with the shaft on the motor, the shaft would get rather toasty and softened the
PETG tensioner arm. After that I used polycarbonate for all the parts, including the gears. This worked well for a long
time, all the way through to the 8:1 version with the TMC2209. At that point, I began doing a lot of printing with this
extruder, and the gears began to crack and fall apart. New gears would only last ~8-12hrs of printing. This is when I
learned that polycarbonate is not a magic all-fitting material. After repeated stresses, the polycarbonate is prone to
cracking. I reprinted the gears out of nylon and the problem went away.

{{ images(images=['brokengears.jpg']) }}

However, the printed nylon gears had problems too. While they held up great, I started noticing a bit of inconsistent
extrusion in the walls of prints. At first, I didn't think much of it, but after watching MirageC's videos on the topic,
I figured the printed gears were likely the main culprit. I don't have a good way to easily make them better to even
confirm that they are the problem, so I bit the bullet and ordered a few sets from Shapeways in SLS nylon. They would be
pretty close to as good as I could get, and should be enough to determine if the gears are a problem. If these solve the
inconsistent extrusion, I know the gears are the problem and I can either use all SLS gears or try to improve my printed
ones. If not then I know not to bother and can look elsewhere. The SLS gears came in a few weeks and I retrofitted them
into one of the extruders. The difference was night and day: wall extrusion became nearly perfect. Now I've kept them
installed and they are still going strong. 

## Tensioner
The tight space constraints extended to the rest of the extruder as well. The tensioner arm needs to hold the second
filament drive wheel under tension to grip the filament strong enough. Most of the time, this is done with a spring
and an adjustment screw to change the tension. Several extruder designs now also feature a quick-release so it's easy to
insert and remove filament.

{{ images(images=['tensioner_closed.png', 'tensioner_open.png']) }}

Once again, there wasn't a lot of space here. I ended up with a bi-stable latch design that wraps around the gearing.
The arm itself acts as the spring, and there is no manual adjustment. Any adjustment comes from printing different
lengths of the top link. I ended up printing a bunch in 0.5mm increments and found which length worked best. I haven't
had to change it since, even for flexible filaments. At some point I might try to make it adjustable, but it isn't
needed right now.

## Electronics
Since there can be up to 4 extruder tools on the printer at a time, I didn't want to have to run 12+ wires back to the
control board for each tool. That adds a lot of potential failure points, and quickly becomes unwieldy to manage.
Instead, I opted to use one of the new CAN bus toolhead boards, in particular the EBB36. These connect to the Raspberry
Pi running Klipper over the CAN bus, and only need 4 wires (+24, GND, CANH, CANL) per tool. The board has a
microcontroller on it along with a stepper driver, heater and fan mosfets, etc. This works as an additional Klipper
MCU, and you can use its IO as if it was on the main control board. This makes all the wiring much simpler and means the
main board can be much smaller as well. Eventually I'd like all the tools to be a CAN board, but some may need to be a
bit more specialized.
