+++
title = "Screw Tool Latch"
date = 2024-09-22
[extra]
image = "screw_latch/screw_latch_printer.jpg"
+++

{{ images(images=['screw_latch_printer.jpg']) }}


The [E3D-inspired tool latch](/projects/3dprinter/toolchanger/latch/) works great smaller tools like extruders and pens,
but struggles with bigger and heavier tools like the spindle tool. When latching the tool from the dock, the latch
needs to pull the tool from where it rests in the dock to where the locating features line it up on the tool face. It
has a limited amount of pulling force to do so, so it often doesn't pull the tool all the way on, resulting in the tool
not being rigidly held. This runs directly into the power limits of the gear motor it uses, especially since it's running
up against hard stops all the time. Increasing the current burns up the windings after a few latches, and a higher gear
ratio results in one of the final gear stages stripping out. This likely could have been solved with some clever software
and hardware tweaks to avoid hitting the hard stops and only apply higher power when needed, but there are a number of
other, smaller problems as well:

1. The limited power, as above
2. The gearmotor is ~$20 each, so experimenting and replacing it often is expensive
3. There's a lathe machined aluminium piece that I can't easily make myself
4. The tool needs to be held in the dock somewhat precisely due to the limited pull force
5. Each tool needs a ramp plate, which needs to be machined. This can be done on the toolchanger, but making one per
   tool can get excessive
6. The ramp plates wear and eventually become less effective and need replacing
7. It's generally finicky to work with

I initially planned to just find a beefier motor, and machine a cycloidal gearbox that should be less prone to stripping.
I got that all designed and prototyped out, but since the gearbox was too small to have bearings everywhere, it just had
too much friction and had slipping issues. It was so close to working, but I didn't see any way past the friction. So,
I abandoned that idea and began looking for new ways to hold the tool. It had to:

1. Be powerful enough to pull the spindle too into the tool face and locate it, even from a poorly aligned dock
2. Hold the tool rigidly every time
3. Use cheap parts
4. All non-off-the-shelf parts made on the toolchanger, ideally printed
5. Simpler features on the tool, since there can be a lot of tools

Something I noticed about the old latch is that the helical ramp plates and the rotating pin act in a similar manner to
a screw, translating rotatory torque into linear force. However, it also acted as the way to engage/disengage the tool
in addition to providing the force to hold it against the tool face. Giving two jobs to one feature at the heart of the
mechanism meant it wasn't great at either of them. Engaging the tool required significant tolerance between the tool and
toolhead sides to allow for misalignment, but this limited the screw action to a coarse thread and only ~1/4 turn. If you
wanted more mechanical advantage out of the 'screw', you gave up on alignment tolerance. Thus, I thought it might be
best to separate the functions and let each function be the best it can be. 

I brainstormed through many possible options, from solenoids to rotary solenoids to magnets to pneumatic cylinders. I
looked at how CNC machines and robotic arms change their tools. For many of them, I couldn't envision them being easily
manufacturable and any better than what I had already. However, there was one design I kept coming back to. I initially
put it off for being too complex and potentially finicky, with more moving parts and somewhat complex interactions
between them. The more I thought about it though, the more it seemed like something I could easily prototype out and
see what happened. So I went for it. What's the worst that could happen?

{{ images(images=['sketch.png']) }}

The general idea was to use a screw with a rotating nut to provide the pulling force, much like a standard linear actuator.
However, to be able to engage and disengage the tool, the screw would need to rotate 90deg so the engagement bar at the
end of the screw could fit in a slot in the tool. I played with having a second actuator just to rotate the screw, but
it would be a lot easier with only one. If the screw was allowed to rotate freely, turning the nut would turn the screw
from friction before it moved the screw linearly. So if we set it up so the screw hits a hard stop after 90deg of rotation,
it would first rotate 90deg, then start pulling in. To unlatch, you just turn the screw in reverse. The friction of the
engagement bar on the tool would keep it locked rotationally, so it would first move linearly out, then once the tool is
free, the friction in the nut would take over and rotate the screw back 90deg. Thus, the screw would be what provides
the pulling force, and the flat engagement bar at the end of the screw engages/disengages with the 90deg rotation. The
functions were seperated and each could be simplified and optimized to do their job better. However, I had a few concerns
before building it:

1. The friction between the screw and the nut is very important to make sure it does the 90deg rotation. If enough friction
    exists elsewhere, the screw could move linearly instead of rotationally, thus failing to engage the tool. If there
    was too much friction with the nut, it would take away from the available pulling force.
2. The whole thing would be bigger. Could it fit in the existing toolhead space constraints?
3. How would the motor drive the nut? Gears? The printed gears in the extruders had their own problems, and with the high
   torque and hard stops I was concerned with damaged teeth. Belts? Does timing belt and pulleys exist that's small
   enough? Non-timing belts could slip, limiting pulling force.

These concerns are what held me off on this idea for so long. In fact, I had thought about this before starting on the
cycloidal drive concept. I wasn't able to prove to myself that these issues, particularly #1, could be overcome. It wasn't
until the cycloidal drive failed that I decided to give it a try and see what happened.

I first did the initial sketch above. This was to lay it all out and show it had some hope of fitting within the space
constraints. I also did some calculations with the motor I was using with the cycloidal gearbox to ensure it could be
powerful enough.

The motor I used is the [PPN13PB11C2](https://www.digikey.com/en/products/detail/nmb-technologies-corporation/PPN13PB11C2/2417080),
with an alternative being the [PPN7PA12C1](https://www.digikey.com/en/products/detail/nmb-technologies-corporation/PPN7PA12C1/2417079)
that's a bit shorter but otherwise the same size. These were the torquiest motors I could find that fit nicely in the
toolhead and aren't insanely expensive. At ~$3 per motor it's not a big deal if I burn a few up. This was originally
chosen for the failed cycloidal drive, so I already had a few and it's a pretty good size already.

Instead of jumping straight to trying to fit it in the toolhead and work with real tools, I designed a standalone prototype 
that would be easier to futz with and iterate on.

{{ images(images=['screw_latch_prototype.png', 'screw_latch_prototype_section.png']) }}

This proved to be very useful. The initial plan was to have the 90deg rotation stops in the tool side, so I didn't include
them in the prototype. While this worked fine, it was annoying to have to always have the tool side available to test it.

Also, in an attempt to solve the friction with the nut, I tried a clutch that would engage between the screw and the nut
only when the screw was fully extended. However, I found this wasn't all that reliable and didn't work that well to begin
with. It actually worked better to let a stationary nut on the screw just run into the rotating nut. The friction between
the screw and the rotating nut was enough to rotate the almost screw every time, and if it ever didn't the nuts running
into each other would ensure it did. The only issue was that it would sometimes tighten itself too much, and the motor
wouldn't be able to unscrew them. This was fixed with a simple spring between the rotating nut and the stationary nut.
This provides more friction when the screw is fully extended, when you want it to turn, and less when the screw was pulled
in, when you wanted the maximum pulling force. It also limited the friction even when fully extended, which the motor
could always overcome.

I went with gears to drive the nut on the prototype, since they are easy to print and enough to get going and at least
test the rest of the mechanism. Luckily, none of my concerns with the gears were founded. They worked great and I stuck
with them.

With the prototype out of the way, it was time to build it into the toolhead for real. Of course, this wouldn't be the
end of it -- I went though many iterations and learned a lot.

{{ images(images=[
    'VID_20240814_234108123.mp4',
    'VID_20240814_233859367.mp4',
    'IMG_20240924_194207281.jpg',
    'VID_20240814_233930601.mp4',
    'IMG_20240924_194219905.jpg',
    'IMG_20240924_194224842.jpg',
    'IMG_20240924_194241917.jpg',
    'screw_latch_cad.png',
    'screw_latch_drive_cad.png',
    'screw_latch_section_cad.png',
    'screw_latch_top_section_cad.png',
    'screw_latch_toolplate_cad.png',
    'screw_latch_printer.jpg',
    'sketch2.png'
]) }}

To start, I designed in 90deg rotation stops to the toolhead, rather than relying on them being in the tool. This
turned out to be not very difficult, both made testing a lot easier and the final result more robust. The stops allowed
the screw to rotate 90deg when it's fully extended, but lock the rotation when it's pulled in.

One of the ongoing issues, though, was that when the screw reached it's limit, the motor would tighten the screw too much,
and it wouldn't be able to unscrew itself later. 

The first step to solve this was to install endstops to cut power to the motor when it reached the end of travel. These
integrated nicely with the rotation lock mechanism, and with a few diodes, I could keep all the wiring on the toolhead
without running the endstop wires back to the controller.

However, this wasn't enough. I did some more digging, and came up with a theory. When the motor runs the screw into the
end, it hits the end and tightens the screw with the torque of the current through the windings and the rotational
inertia of the rotor and gears spinning. But when it tried to loosen it later, it was only acting with the torque from
the current through the windings. Thus, it was tightening the screw with more torque than it was able to loosen it with.
The endstops reduced the winding torque, but did nothing about the rotational inertia. I would need to figure something
out so when the motor starts up with the screw tightened, it could get up to speed before attempting to unscrew it.

I considered making a tiny little centrifugal clutch or impact driver mechanism, but those would be difficult to fit in
such a small space. So I ended up with a much simpler solution. I decoupled the nut and it's driving gear, so they
deliberately had one full rotation of backlash between them. This meant that after tightening, when the motor changed
direction to loosen it, it had one rotation of the nut gear to get up to speed, and the inertia from the rotor spinning
would apply to unscrewing the screw. It effectively got just one blow from an impact driver, but that was all it needed.
With that change in place, it was able to unscrew itself almost every time. From there, tweaking the spring force and
some washers between the nuts reduced the friction enough to work every time.

{{ images(images=['screw_latch_gear.png']) }}

Another issue I ran into was failing limit switches. After just a few minutes of latch and unlatching, the fully out
limit switch would fail to conduct current. Manually actuating the switch it a few times was enough to get it going
again, but it would fail soon after. After some futzing, I figured it out. This limit switch was hit straight on, after
the 90deg rotation. The other limit switch got hit by the target sliding over it from the side. The straight on hit
would cause the motor to turn off, but the springiness of the switch would bounce the screw back, re-enabling the motor,
and hitting the switch again. This bouncing caused many, probably hundreds, of switch actuations to happen in the span of
a second or so, wearing out the switch contacts from arcing. Installing decoupling capacitors on the switches and motors
solved this problem. I also did some research on limit switches, and found the ones I was using weren't rated for the
current the motor was drawing, so I got some that were rated for more current in the same form-factor.

I got this all buttoned up and installed on the toolchanger. It's a bit finicky to get the engagement bar tightened on
the end of the screw at the right angle and distance from the tool face, but it worked out. Once in a while it would
still fail though, sometimes even for the same tightening-too-tight to unscrew issue I thought I had fixed before.

This time, I decided to play around with the voltage applied to the motor. I had discounted this before, since it was
still running off the constant-current driver I was using with the old latch, I figured that current would set the
torque it tightened at and avoid burning out the motor. The motor itself is rated for 7.5v, but since the rest of the
printer is 24v, I was running off 24v with the constant current driver limiting the stall current so it wouldn't burn up.
However, the motor's speed increases ~linearly with voltage, but the rotational inertia increases with speed^2. By slowing
the motor down, we can significantly reduce the rotational inertia, but keep the tightening torque from the winding
current. I tested this by adding a buck-boost converter to power the motor. I started at 7.5v, and the problem went away.
However, this did result in reduced holding force on the tool. I slowly increased it 1 volt at a time until it started
failing, and found that at 18v, it was very reliable and still had plenty of holding force. This finally solved this 
problem

The other problem causing failed latches (and particularly unlatches) was not tightening the engagement bar on the tool
enough, so it would rotate itself over time relative to the tool, causing all kinds of problems. Since it's a printed
part with plastic threads tightened against a real nut, it could only get so tight. I eventually gave up and superglued
it on before eventually making one out of brass that could be tightened more than enough.

With all that out of the way, the latch has yet to fail. It also holds the tool much tighter than the old latch, and has
a much higher tolerance for where the tool sits in the latch. Combined with a second x-axis linear rail, the whole machine
is much more rigid. The 6mm GT2 timing belts are the next bottleneck. Maybe it's time for 9mm belts...

