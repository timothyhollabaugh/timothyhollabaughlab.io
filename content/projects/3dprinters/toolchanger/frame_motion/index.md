+++
title = "The Frame and Motion"
date = 2023-09-18
+++

# The Frame
I started by going between mostly printing the frame (like my PCB mill), using aluminum plates like the E3D toolchanger,
or doing it all out of aluminum extrusions like the Voron printers. The printed frame I though might be the cheapest, but
a total noodle and the most jank. The aluminum plates could work if I figured out somewhere to route out the plates, or
maybe try to piece it together from smaller places I could route on my PCB mill. Doing it all out of extrusions I didn't
really consider until later on since I had a deep-seated fear of the usual extrusion joining fasteners. They're always
either not very sturdy and loosen up or overpriced, and can be difficult to get square and lined up. It wasn't until I
found the Voron printers and their blind-joint method that I seriously considered it.

For the motion, I had initially thought about using smooth rods. Both my Mendel and PCB mill used 8mm smooth rods, so I
was familiar with how they worked. However, that also meant I was familiar with how mush of a wet noodle they can be.
I wanted something stiffer than those. I briefly considered going with 10 or 12mm rods, but I didn't know how much better
they would be. Eventually I settled on MGN12 linear rails. They're a little more expensive, but are much stiffer and
easier to work with.

The rough overall dimensions of the printer was figured out before I decided which frame to use. I started with the tool size:
the spindle on my mill is 50mm diameter, the E3D tool change mechanism is ~50mm wide, and you could easily fit a V6 hotend
with some cooling fans in 50mm. So I decided the tools would fit in a 50x50mm square, and worked outwards. Clearly this
would be plenty and a tool would never need to be larger than this _(omnious foreshadowing intensifies)_. Anyway, this meant
fitting 4 docked tools side-by-side would be at least 4*50mm = 200mm, which is conveniently the same as the required bed
width. If I wanted to make the frame for the motion system out of a single aluminum plate, it would be convenient if it all
fit on a 12x12in plate. This put the maximum outer dimensions at 300mm. If the X carriage is the same 50mm wide as the tools,
250mm of total X travel is need. This gives a nice 25mm of space on each side for the Y rails, which works well. The Y
axis would need about 50mm for the tool docking, plus the 150mm bed depth and the 50mm carriage width, worked out to also
be 250mm.

When it came to choosing the frame structure, the first option to eliminate was the plates. They were more expensive and
harder to machine into the right shape, and aluminum extrusions provided most of their rigidness. Once that was out, I
initially assumed the extrusions would be a decent amount more expensive than printing it. However, I priced out the
extrusions from MISUMI and approximated the amount of filament it would take to print. It turned out that they were both
about the same cost. This made the choice obvious: the extrusions were much more rigid and less jank, with no real downside.

I was planning on sticking with the initial plan of 300mm outer dimensions, and using 250mm MGN12 rails for the X and Y.
However, finding 250mm MGN rails of decent quality was proving rather difficult. 300mm rails were much more common. Since
I wasn't using the plates anymore, the outside dimensions could be expanded a bit to accommodate. This also gave a little
breathing room around the docked tools and the width of the X carriage. And maybe I could fit a 5th tool in there if they
squished in enough?

Like the E3D toolchanger, I wanted the entire CoreXY motion system to be entirely attactched to the top rectangle of
extrusions, so it could be taken off the rest of the printer all in one piece. Since I wasn't super familiar with the
CoreXY layout, it would also let me build up just the XY motion and some of the toolchanging bits first to make sure it
all worked before building the rest of the printer.

# The Z Axis
The Z axis doesn't need to be anything special. It just needs to move the bed up and down, and be reasonably sturdy. The
original plan was to do a cantilievered design, and put some Z smooth rods under where the AB motors go, so they don't
take extra space. In college, I worked with several Ultimakers, which have a similar cantilivered bed. However, those
beds aren't the most rigid and one in particular was always loose. When I saw the Voron V1.8 with the dual lead screws,
I figured that was the way. I also wanted to go with the thickest smooth rods I could fit, which ended up being some fat
20mm ones. There's only about 100mm of Z travel, so it's very sturdy.