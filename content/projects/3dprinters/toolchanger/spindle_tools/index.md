+++
title = "Spindle Tools"
date = 2024-08-29
[extra]
image = "spindle-tools/spindle_cad.jpg"
+++


{{ images(images=[
    'spindle_cad.png',
    'spindle.jpg',
    'IMG_20240829_221212895.jpg',
    'IMG_20240829_221226260.jpg',
    'IMG_20240829_221301326.jpg',
    'IMG_20240829_221344642.jpg',
    'IMG_20240829_221403099.jpg',
    'IMG_20240829_221713914.jpg',
    'IMG_20240829_221748170.jpg',
]) }}

[CAD in OnShape](https://cad.onshape.com/documents/f2777db2ae5cfbb4c1fdab1a/w/00f08890d73b96b8f53d1600/e/50c4f3b380a340cca4096157?renderMode=0&leftPanel=false&uiState=66f06ad8e01afe4bbd843458)


Two of the goals for the toolchanger is to do both printing and milling in the same job and to mill PCBs. Both of these
require a spindle tool. Being a toolchanger tool, this spindle has a few unique requirements:

1. Fit in a 50mmx50mm tool footprint, including the mounting toolplate (mostly)
2. Not too heavy
3. High RPM for small tools
4. Fit 1/8in and maybe 1/4in tools
5. Enough torque to not be the limiting factor

The first two points immediately rule out any off the shelf spindle I could find. The smallest cheap spindles out there
tend to be 2in diameter, or 50.8mm. This should be close enough, right? Well, no. The tool dock arms are exactly 50mm
apart, so 50.8mm would spread them out when docking the tool, which isn't good. I'd need to adjust the tool dock
spacing, and change all the existing tools in the process. It's doable, but not ideal. However, there's a bigger
problem. The 50mmx50mm footprint needs to include the tool mounting plate, with the locating cap nuts and tool latch
features. This eats into the 50mm by about 10mm, leaving only 40mm for the tool. There was no way the 50.8mm would fit
there. 

This meant I would be designing my own spindle. I had done this before for my dedicated PCB mill with a drone motor
and an ER11 extension collet chuck. That worked good, but the motor was a bit underwhelming and the connection between
the motor and the ER11 extension was never great. It was eventually replaced by a Tormach modular ER11 collet chuck
with an M8 thread that I could connect to the motor with an M8 to M5 thread adapter. 

Do for this one, I decided to go with another drone motor and re-use the ER11 extension. Someone gave me a box of old
drone parts that included a bunch of motors a bit bigger than the ones I was using before with matching ESCs, so I
figured that would be a good start. The motors are all black and unbranded, so I'm not really sure what they are. The
rotor size measures 22mm x 17mm, so it's probably similar to other 2217 or similar sized motors out there. To connect
to the ER11 extension, I decided to try a belt this time. The extension is long enough that the motor can overhang the
back of the tool, and sit partially above the toolchanger's toolhead when loaded. The pulleys form about a 2:1 gear
reduction, so that should provide a decent bump in torque as well. To attach pulley to the motor, I didn't want to
use the M5 prop mount thread, since the pulley needed to be well-balanced on the shaft and screw threads tend to be
slightly undersized. This motor has a 4mm internal shaft, but it doesn't stick out enough to mount the pulley to. I
ended up pressing the 4mm shaft out and replacing it with a longer one that sticks out the bottom.

{{ images(images=['spindle_cad_section.png']) }}

This spindle works well for what it is. It can cut though wood and plastic no problem, and even aluminum and brass
with light cuts.

{{ images(images=['adaptive.mp4']) }}

After a while, the motor started making a lot more noise than usual. Taking it apart, I noticed some wear on the new
shaft:

{{ images(images=['motor_shaft.jpg']) }}

This lines up with where the bearing in the stator is. With all the vibration from milling, the shaft must have had just
enough play to wiggle up and down slightly, causing the wear. I went back and checked the McMaster page for the shaft,
and it turned out to be stainless steel, not hardened steel like I was expecting. I went back and ordered a new hardened
steel shaft (which McMaster lists under linear shafts, not rotary shafts), and it hasn't shown any signs of noise or
wear since.
