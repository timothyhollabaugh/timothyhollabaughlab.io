+++
title = "The Tool Latch"
date = 2023-09-18
[extra]
    image = "latch/latch2section.png"
+++

# The Tool Latch
This was probably the most critical part of the design. If the tool latch mechanism didn't work, the whole thing wouldn't
work. As such, I tried out a few options before designing the rest of the printer. I particularly liked the E3D mechanism,
since the ball and pins could locate the tool accurately. However, I didn't have a lathe and didn't know how I could make
the shaft for the tool latch itself, and the spring-loaded latch with the stepper motor seemed a bit odd. I definitely
wanted to keep the balls and pins.

My first thought was to use a solenoid for the latching instead of the stepper motor. If I turned all the balls so they
pointed outwards, I could put the top one on a lever actuated by the solenoid. I also went with rounded cap nuts instead
of balls since they're easier to mount and readily available.

{{ images(images=[
'latch1cad.png',
'latch1section.png'
]) }}

This version I ended up printing before the rest of the printer to see if it would work at all. It did a decent job of
gripping the tool plate (ring?), but it turned out that having one of the locating balls on the moving lever wasn't
ideal. The locating ball had to be in the same spot every time so the tool was in the same place every load. However,
this meant the hinge joint had to be absolutely perfect, with zero play. As printed, it definitely did not have zero
play. In fact, there was quite a bit of play. Making this good would be quite involved with 3d printing the parts, which
is not ideal. I decided that having the locating balls move was a no-go.

I went back to the E3D tool latch where the 3 locating balls are all fixed, and there's a shaft in the middle that turns
a cross-pin against a set of ramps on the tool. The initial issue was that I couldn't easily make the shaft, since it
has several cross-holes and other features. What if I could design the shaft around a standard screw? I put this idea
together and tried it out. The cross-pin for latching was replaced with a printed part with helix ramps opposite the
ones on the tool, and fit over a hex head on an M4 bolt. The other end had a set of gears like the E3D one, but to a
brushed gearmotor instead of a stepper. In the middle was the same spring, and a set of bronze bushings to allow the
screw to both rotate and slide in and out with the spring.


{{ images(images=[
'latch1.5cad.png',
'latch1.5section.png'
]) }}

However, this one also had issues after printing it out. The main one was that the M4 bolts are rather smaller than 4mm
at the shoulder, making them a loose fit in the bushings. I would need to find a proper shoulder bolt, or use a real 4mm
shaft instead of a screw, both of which are more expensive and harder to find. Also, I didn't really like how the gears
had to slide against each other when latching and the shaft gets pulled into the tool. Printed gears there would mean
sliding against the layer lines, so it wouldn't be very smooth.

After that, I tried for one more idea, hopefully that's all printable without tight tolerance requirements. I decided to
stick with the E3D shaft-and-crosspin latch since it works well for them. However, I wanted to eliminate the spring and
the gears. I thought about what the spring and stepper motor were doing here. The 3 locating balls make contact with the
6 locating pins on the tool to provide 6 points of contact, fully constraining the tool. For this to work, there needs
to be a force applied perpendicular to the toolplate to hold all 6 points in full contact. This is what the spring does.
Also, the contact points of the cross-pin on the ramps aren't in a precise location, so the spring lets the cross-pin
adjust to where the ramps are.

What if I could combine the spring and the stepper motor? A brushed DC motor acts somewhat like a spring at constant
current, providing a constant(-ish) torque output. With this in mind, it seems kind of silly to use a stepper motor with
a spring. Stepper motors are good at precise positioning, but bad at torque control since they loose steps if their
toque limit is exceeded. However, what we actually want is a constant torque (or force), and the spring converts the
stepper's position into a force. DC motors allow us to control the torque directly.

If the latch shaft was driven by a DC motor at constant torque, it would turn until the cross-pin engaged with the ramps
on the tool and stop there. In a zero-friction world, this would also provide a constant-force pull on the tool to keep
the locating balls in contact with the locating pins. However, the angle of the ramps on the pins is steep enough that
friction becomes a dominant force. The cross-pins on the ramps will act like a screw here. Once they are turned in
place, friction will keep them there, much like tightening a nut on a screw. The motor torque will determine how much it
gets tightened. In a way, the rest of the mechanism will act as a very stiff spring keeping the friction there. To
unlatch the tool, the motor needs to overcome this friction force to turn the pins back.

To test this out, I came up with a design that features a 3d printed "shaft" that holds the cross-pin, fits over the
motor shaft, and the whole thing fits in a larger bearing. This part also features one non-rounded corner on it's flange
to act as a hard stop, so unlatching just means running constant current in the opposite direction.

{{ images(images=[
'latch2cadplate.png',
'latch2sectionplate.png',
'latch2cad.png',
'latch2section.png',
'IMG_20230615_220425298.jpg',
'IMG_20230615_220443722.jpg',
'IMG_20230615_220639182.jpg',
'IMG_20230615_221107115.jpg',
'IMG_20230615_221113867.jpg',
'IMG_20230615_222051506.jpg',
'IMG_20230615_222101979.jpg',
'IMG_20230615_222314443.jpg',
'IMG_20230618_232001762.jpg',
'IMG_20230618_232229680.jpg',
]) }}

Printing the "shaft" would be a difficult print. The inner motor shaft hole and the outer surface for the bearing need
to be concentric, and the load is directly against the layers. I tried this out witn a 0.15 nozzle in PC, and it worked
reasonably well. This was the most promising design to-date and actually seemed to work. At this point I decided to go
ahead with the rest of the printer.

However, this design is not without it's flaws. I soon found that the motor would often bind up even with no load. It
turns out the printed "shaft" isn't concentric enough between the motor shaft and the bearing. No matter how I tried to
re-print it, it wouldn't be good enough. I eventually caved and sent it out to Shapeways to SLS print it, and that
worked much better, and I used this one for a long time. Eventually, I decided even the SLS one was a bit too stretchy
to rigidly hold the tool, so I got a buddy of mine to lathe one out of aluminum, which turned out to be much stiffer.
So much for not needing a lathe...

I also ended up milling a brass toolhead plate with the locating pins to stiffen everything up as well. This one done on
my PCB mill, and came out well. It also makes the whole thing much stiffer. At some point I may do the tool plate too.

The ramp part on the tool at the latch cross-pin latches against was initially also milled from brass on my PCB mill.
It's important that the ramps are very smooth for the cross-pin, and I didn't think I could do that with a printed part,
since there would always be stair-stepping from the layer lines. With the CNC, I could use helical arc g-codes to get
the ramps perfectly smooth. These worked great. However, I didn't want to break out the mill for every new too I put
together, so I ended up printing some anyway. With the 0.15mm nozzle and a 0.05mm layer height, the layers are small
enough to be smoothed out with a file. The printed ones seem to work just as well as the brass ones.

