+++
title = "Spindle ESC"
date = 2020-11-05
+++

To run the spindle of my PCB mill, I have a small brushless DC motor directly
coupled to an ER11 collet. It is a 1900Kv EMAX drone motor, and it works well
enough, but it runs a bit fast and is underpowered for larger cuts.

Part of what I wanted was 

