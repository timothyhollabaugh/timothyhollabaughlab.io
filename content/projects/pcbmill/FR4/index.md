+++
title = "Milling FR-4"
date = 2020-04-10
+++

Professional circuit boards are often made of a material known as FR-4. This is a
fiberglass weave infused with expoxed, laminated with a layer or two of copper.
The fiberglass-expoxy substrate provides good electrical and mechanical properties
for pcbs.

At first, I milled FR-4 blanks. This worked pretty well, but I quickly found out that
milling FR-4 produces fine fiberglass dust. It turns out that breathing fiberglass is
not exactly the greatest, so I quickly switched to using FR-1. FR-1 is a paper-based
material, with no fiberglass, so there are not harmfull particles preduced. However,
FR-1 has it's own problems. The copper does not stick to the board nearly as well, so
you end up with traces peeling off quite a bit, amoung other things.

So, I started looking for ways to safely mill FR-4. I noticed that Bantam Tools sells
an FR-4 kit for their mill, where they mill FR-4 submerged in minerial oil. The oil
traps all the dust, so there are no harmfull particles to breath in. Well, I can make
a tub to put oil in. I designed and printed out, and it worked pretty well. All the
dust is nicely trapped in the oil, with nothing escaping.

I initially started using engine oil, since that is what my dad had around. However,
it is not exactly the easiest to work with. It smells, and it a bit opaque. I then
switched to using canola oil, as used for cooking. It's more clear, and has no ordor,
yet still traps the dust the same.

To hold down the board under the oil, I started with the same double-sided tape I
was using before. However, the tape has a few problems. To start, the tape absorbs
a bit of the oils, and gets pretty nasty to deal with. Secondly, tape does not stick
to oil This means that the tub and the PCB need to be thoughly scrubbed every time
new tape is applied.

Trying to hold down a PCB under oil is not the easiest. There cannot be any penetrations
in the tub, so I first tried some clips that wrapped around the walls of the tub. This
avoided any holes, and held the board down reasonably well. However, not quite well
enough. Once the FR-4 blank has a bunch of boards cut out, it gets a bit more bendy than
it was before. With the clips pushing in on the edges, this would cause the center of the
board to bow upwards. While the bed leveling would take care of the bow no problem,
without anything underneath it, cuts would not cut deep enough.

To solve this problem, I went with the most secure way I could think of: screwing the
thing down. Since there could not be any penetrations, all the holes had to be both
blind and threaded. To do this in a 3d printed part, I used some aluminum press-fit
threaded inserts. They press into a 4mm hole, then expand and grab the plastic once
a screw is inserted. They worled pretty well. Another benifit of the screws is that
they provide somewhat repeatable prositioning, which makes it possible to line up
double-sided board properly.

